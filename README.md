[![pipeline status](https://gitlab.com/ignatiusmb/ppw-crowdfunding/badges/master/pipeline.svg)](https://gitlab.com/ignatiusmb/ppw-crowdfunding/commits/master) [![coverage report](https://gitlab.com/ignatiusmb/ppw-crowdfunding/badges/master/coverage.svg)](https://gitlab.com/ignatiusmb/ppw-crowdfunding/commits/master) [![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/) [![MIT Licence](https://badges.frapsoft.com/os/mit/mit.png?v=103)](https://opensource.org/licenses/mit-license.php)

# Crowd Funding Project
Repository for a Crowd Funding Website, Project 1 Web Design & Development, Group 4 from Class A  
  
[SEE OUR PROJECT HERE](https://ppw-crowdfunding.herokuapp.com)

## Developed by:
- Ignatius Bagussuputra
- Muhamad Abdurahman
- Rd Pradipta Gitaya Samiadji
- Revan Ragha Andhito