from django.contrib import admin
from django.urls import include, path
from django.conf.urls import handler404, handler500
from crowdfunding.views import handler_404, handler_500

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('crowdfunding.urls')),
    path('donasi/', include('donation.urls')),
    path('berita/', include('news.urls')),
    path('program-donasi/', include('programdonasi.urls'), name='program-donasi'),
    path('auth/', include('social_django.urls', namespace='social')),
]

handler404 = handler_404
handler500 = handler_500
