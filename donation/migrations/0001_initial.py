# Generated by Django 2.1.2 on 2018-10-17 07:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='donation_list',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('program_name', models.CharField(max_length=1000)),
                ('donator_name', models.CharField(max_length=255)),
                ('donator_email', models.EmailField(max_length=255)),
                ('donator_amount', models.PositiveIntegerField()),
                ('donator_showname', models.BooleanField(default=True)),
            ],
        ),
    ]
