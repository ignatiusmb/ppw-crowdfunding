from django.db import models
class NewsModel(models.Model):
    nama = models.CharField(max_length=50)
    lokasi = models.CharField(max_length=50)
    jumlah_korban = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=1000)
    img_url = models.CharField(max_length=200)
