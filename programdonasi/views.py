from django.contrib.auth.models import User
from django.shortcuts import render
from donation.models import donation_list

from .models import ProgramModels
from assets.others.navbar_content import navigation_content

def program_donasi_view(request):
    response = {
        "navigation" : navigation_content,
    }
    html = 'programdonasi.html'
    our_program = ProgramModels.objects.all()
    response['all_program'] = our_program
    return render(request, html, response)

def program_details_view(request, my_id):
    my_program = ProgramModels.objects.get(id=my_id)
    html = 'programdetails.html'
    total = 0
    try:
        donations = donation_list.objects.filter(program_name=my_program.nama)
        for donation in donations:
            total+=donation.donator_amount
    except donation_list.DoesNotExist:
        donations = None
    context = {
        'this_program': my_program,
        'donations': donations,
        'total':total
    }
    return render(request, html, context)
