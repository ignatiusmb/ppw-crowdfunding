from django.urls import path
from programdonasi import views

urlpatterns = [
    path('', views.program_donasi_view, name='program-donasi'),
    path('<my_id>/', views.program_details_view, name='details')
]
